# README #
If you use this code or these concepts please credit me as appropriate. 


nnMemory is a proof of concept I had a while ago. The basic idea is to use something similar to attention model to select an expert layer, or layers, that will be swapped in to a network based based on the current data being processed. There are three major parts to this:

A selector - This is a small conventional layer layer inside your network. It is built and trained like any other layer.

Descriptors -  These are identifiers that will be compared against the selector in order to find the best knowledge to use

Expert Knowledge - In the simplest implementation this is the weights/biases that will be swapped in.

Both the expert knowledge and the selector are trained exactly as you would expect in a normal network. The descriptors however are trained by learning to be more like the selector that choose them. From this basic idea many things can be done.

### What is this repository for? ###

nnMemory provides two main benefits:

1) It solves something similar to the pigeon hole problem for a network. Basically, how can I learn from more data if I have a fixed network size? Answer: You can't without loosing something. Your network must grow to gain more knowledge. nnMemory allows for that growth. Since I am looking for similarity in descriptors it doesn't matter if I have 2, 2 thousand or 2 million. I can grow the expert knowledge base as more data comes in. This growth effectively allows me to keep learning. Think of the bigger problem this way. When a network first starts training all examples are new and likely require structural changes to adequately account for the unique information each example brings in. As you see more data the amount of 'new' information from each example grows smaller and smaller, but never completely goes to zero. This means that if you have a fixed size network then you will eventually run out of space to encode unique new information. nnMemory solves this by allowing you to add more expert knowledge as more data comes in. How you do this is a topic that could cover many papers but in practice I do two things. First, I grow the expert knowledge at a rate of c*log(total examples seen). This should match a similar curve to the true underlying unique information in the stream of examples being processed. Next, when I create a new row of expert knowledge I use the average of all selectors/expert knowledge data. This generally results in the new row quickly taking over some small feature space and integrating in with a minimum of disruption to the already established expert knowledge. In many ways this is like applying k-means to an infinite dataset that presents its self one example after another. 

2) nnMemory allows knowledge transfer between different networks by using shared expert knowledge. Like task based learning nnMemory allows you to build networks that train to do one task but then transfer that knowledge to a different task. Unlike task based learning nnMemory doesn't need to share identical inputs. In other words, nnMemory allows you to share deep features. All you have to do to share knowledge is to create a selector with the same size in a new network and point it at the shared expert knowledge. Now you can learn from ANY type of data and ANY type of task and apply it to ANY other type of data or task. For obvious reasons, this is an important thing.

### How do I get set up? ###

This project is a proof of concept. The code is rough with a lot of dead ends. Many of the concepts I talked about above are not currently in this code-base. Download the project as a whole and look at testing.py. It isn't good code but hopefully you see what is going on. tfBaseline is just there for testing purposes to help be debug. One thing to note, this example is NOT better than the simple feed forward baseline. It is just being used to make sure the ops work well and the network and training have not been optimized for nnMemory.

### Contribution guidelines ###

I do not currently allow outside contributors but I may do that soon. if you want to contribute send me an e-mail at wallyduck@yahoo.com