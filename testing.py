import os, math

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
os.environ['MLP_DATA_DIR'] = '../School/MLP/MLP_DATA/'

import tensorflow as tf
import nnmemory
from tensorflow.examples.tutorials.mnist import input_data
from collections import Counter
import numpy as np
from mlp.data_providers import CIFAR10DataProvider, CIFAR100DataProvider


class TestingEnvironment:
    def __init__(self, batchSize=50):

        self.dataSources = {}
        self.dataSources["cifar10_train"] = CIFAR10DataProvider('train', batch_size=batchSize)
        self.dataSources["cifar10_valid"] = CIFAR10DataProvider('valid', batch_size=batchSize)
        self.dataSources["cifar100_train"] = CIFAR100DataProvider('train', batch_size=batchSize)
        self.dataSources["cifar100_valid"] = CIFAR100DataProvider('valid', batch_size=batchSize)
        self.resetTrain("cifar100")
        self.resetValid("cifar100")

    def resetTrain(self, dataSet, setDefault=True):
        self.dataSources["{}_train".format(dataSet)].reset()
        if setDefault:
            self.train = self.dataSources["{}_train".format(dataSet)]

    def resetValid(self, dataSet, setDefault=True):
        self.dataSources["{}_valid".format(dataSet)].reset()
        if setDefault:
            self.valid = self.dataSources["{}_valid".format(dataSet)]


def counterPercentages(counter, experts):
    counts = np.asarray([counter.get(i, 0) for i in range(experts)], dtype=np.float32)
    counts = counts / counts.sum()
    return [i for i in counts]


def main():
    EXPERTS = 9
    BATCH_SIZE = 100

    sess = tf.InteractiveSession()
    # dataSet = input_data.read_data_sets('MNIST_data', one_hot=True)
    dataSet = TestingEnvironment(batchSize=BATCH_SIZE)

    SELECTOR_WIDTH = 16
    TOTAL_WIDTH = 256
    EXPERT_WIDTH = TOTAL_WIDTH - SELECTOR_WIDTH
    CLASS_COUNT = dataSet.train.num_classes
    INPUT_DIM = dataSet.train.inputs.shape[1]
    SENSITIVITY=0.0

    x = tf.placeholder(tf.float32, [None, INPUT_DIM])
    labels = tf.placeholder(tf.float32, [None, CLASS_COUNT])
    sensetivity = tf.placeholder(tf.float32)
    knowledgeSize = tf.placeholder(tf.int32)

    # Define the selector layer
    selectorWeights = tf.Variable(tf.truncated_normal([INPUT_DIM, SELECTOR_WIDTH], stddev=0.1))
    selectorBias = tf.Variable(tf.zeros([SELECTOR_WIDTH]))
    selectorLayer = tf.nn.relu(tf.matmul(x, selectorWeights) + selectorBias)

    # Create the expert descriptors. These will be used to compare against the selector layer to find the most
    # relevant expert knowledge to inject
    expertDescriptors = tf.Variable(tf.truncated_normal([EXPERTS, SELECTOR_WIDTH], stddev=0.1))
    # The weights and biases represent the expert knowledge payload so create one for each expert
    expertKnowledgeWeights = tf.Variable(tf.truncated_normal((EXPERTS,) + (INPUT_DIM, EXPERT_WIDTH), stddev=0.1))
    expertKnowledgeBiases = tf.Variable(tf.zeros((EXPERTS,) + (EXPERT_WIDTH,)))
    # determine which expert to use based on how similar its descriptor is to the selector
    knowledgeIndices, descriptorLoss = nnmemory.selectProbabalisticExpertsOP(selectorLayer, expertDescriptors[0:knowledgeSize], sensetivity)
    #knowledgeIndices, descriptorLoss = nnmemory.selectExpertsOP(selectorLayer, expertDescriptors, EXPERTS)
    expertLayer = nnmemory.expertLayer(x, knowledgeIndices, expertKnowledgeWeights, expertKnowledgeBiases, EXPERTS)

    # The complete layer is a combination of the selector and the expert knowledge
    layer1 = tf.concat([selectorLayer, expertLayer], axis=1)
    # Last layer to predict the logits based on the combined selector/expert layer
    outputWeights = tf.Variable(tf.truncated_normal([TOTAL_WIDTH, CLASS_COUNT], stddev=0.1))
    outputBiases = tf.Variable(tf.zeros([CLASS_COUNT]))
    logits = tf.matmul(layer1, outputWeights) + outputBiases

    # standard outputs
    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=labels))
    train = tf.train.GradientDescentOptimizer(.1).minimize(loss)
    trainDescriptors = tf.train.GradientDescentOptimizer(.1).minimize(descriptorLoss)

    accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(logits, 1), tf.argmax(labels, 1)), tf.float32))
    sess.run(tf.global_variables_initializer())

    totalIndicesUsed = Counter()
    indicesUsed = Counter()

    lastStep = 0
    lastEval = 0
    lastSubStep = 0

    EPOCHS = 100
    SUB_STEP_SIZE = BATCH_SIZE * 100
    STEP_SIZE = SUB_STEP_SIZE * 10
    EVAL_SIZE = STEP_SIZE * 2
    i = 0
    currentKnowledgeSize = 2
    for epoch in range(EPOCHS):
        for batch in dataSet.train:
            lastEval += BATCH_SIZE
            lastStep += BATCH_SIZE
            lastSubStep += BATCH_SIZE
            fd = {x: batch[0], labels: batch[1], sensetivity:SENSITIVITY, knowledgeSize:currentKnowledgeSize}
            _, _, indices, expertDescriptorsData, lossData = sess.run(
                [train, trainDescriptors, knowledgeIndices, expertDescriptors, descriptorLoss], feed_dict=fd)

            # now do the custom learning for the descriptors
            for indice in indices:
                totalIndicesUsed[indice] += 1
                indicesUsed[indice] += 1

            lastEval += BATCH_SIZE
            lastStep += BATCH_SIZE
            lastSubStep += BATCH_SIZE

            if lastEval >= EVAL_SIZE:
                # train_accuracy = accuracy.eval(feed_dict={x: dataSet.valid.inputs, labels: dataSet.valid.targets, keep_prob: 1.0})
                fd[sensetivity] = 0.0
                acc = 0.0
                numBatches = 0
                for batch in dataSet.valid:
                    numBatches += 1
                    fd[x] = batch[0]
                    fd[labels] = batch[1]
                    acc += accuracy.eval(feed_dict=fd)
                train_accuracy = acc / numBatches

                print("{}, {}, test accuracy {:0.2f}, counts: {}".format(epoch, i + BATCH_SIZE, train_accuracy * 100.0,
                                                                         counterPercentages(totalIndicesUsed, EXPERTS)))
                lastEval -= EVAL_SIZE
                indicesUsed = Counter()
                totalIndicesUsed = Counter()
            elif lastStep >= STEP_SIZE:
                fd[sensetivity] = 0.0
                fd[x] = batch[0]
                fd[labels] = batch[1]
                train_accuracy = accuracy.eval(feed_dict=fd)
                print("{}, {}, training accuracy {:0.2f}, counts: {}".format(epoch, i + BATCH_SIZE,
                                                                             train_accuracy * 100.0,
                                                                             counterPercentages(totalIndicesUsed,
                                                                                                EXPERTS)))
                indicesUsed = Counter()
                totalIndicesUsed = Counter()
                lastStep -= STEP_SIZE
            elif lastSubStep >= SUB_STEP_SIZE:
                print("{}, {}, counts: {}".format(epoch, i + BATCH_SIZE, counterPercentages(indicesUsed, EXPERTS)))
                lastSubStep -= SUB_STEP_SIZE
                indicesUsed = Counter()
            i += batch[0].shape[0]
        if math.log1p(epoch + 1) * 1.5 > currentKnowledgeSize and currentKnowledgeSize < EXPERTS:
            nnmemory.grow(expertDescriptors, currentKnowledgeSize, sess)
            nnmemory.grow(expertKnowledgeBiases, currentKnowledgeSize, sess)
            nnmemory.grow(expertKnowledgeWeights, currentKnowledgeSize, sess)
            currentKnowledgeSize += 1


if __name__ == "__main__":
    main()
