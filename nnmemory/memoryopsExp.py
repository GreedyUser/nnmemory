import numpy as np
import tensorflow as tf
from sklearn import preprocessing
from tensorflow.python.framework import ops


def broadcastMatmulPlus(a, b, c):
    # return tf.map_fn(lambda m: tf.reshape((tf.matmul(tf.reshape(m[0], (1, -1)), m[1])) + m[2], (width,)),
    # (a, b, c),
    # dtype=tf.float32)

    shape = tf.shape(a)
    rank = shape.get_shape()[0].value
    a = tf.expand_dims(a, rank)
    vm = tf.multiply(a, b)
    return tf.reduce_sum(vm, rank - 1) + c

# py_func code modified from Harpoon's example which can be found at:
# https://gist.github.com/harpone/3453185b41d8d985356cbe5e57d67342
# Define custom py_func which takes also a grad op as argument:
# Def custom square function using np.square instead of tf.square:

def py_func(func, inp, Tout, stateful=True, name=None, grad=None):
    # Need to generate a unique name to avoid duplicates:
    rnd_name = 'PyFuncGrad' + str(np.random.randint(0, 1E+8))

    tf.RegisterGradient(rnd_name)(grad)
    g = tf.get_default_graph()
    with g.gradient_override_map({"PyFunc": rnd_name}):
        return tf.py_func(func, inp, Tout, stateful=stateful, name=name)


def selectExpertsTFOP(selector, descriptors, name=None):
    args = [selector, descriptors]
    with ops.op_scope(args, name, "select_experts") as name:
        fnc = py_func(_selectExperts,
                      args,
                      [tf.int32],
                      name=name,
                      grad=_selectExpertsGrad)  # <-- here's the call to the gradient
        return fnc[0]


def _selectExperts(selector, descriptors):
    normDesc = np.transpose(preprocessing.normalize(descriptors, norm='l2'))
    normSelector = preprocessing.normalize(selector, norm='l2')

    selectorSimilarity = np.matmul(normSelector,
                                   normDesc)

    # Now test to see which was the most similar
    knowledgeIndices = np.argmax(selectorSimilarity, axis=1).astype(np.int32)
    return knowledgeIndices


def _gradCalc(element):
    selectors = element[0]
    descriptor = element[1]
    f1 = lambda: (descriptor - selectors) / (tf.shape(selectors, out_type=tf.float32)[0])
    f2 = lambda: tf.zeros(tf.shape(descriptor))
    gradient = tf.to_float(tf.case([(tf.size(selectors) > tf.constant(0), f1)], default=f2))
    return gradient


def _selectExpertsGradOp(inputs, outputs, expertCount):
    print("selectExpertsGrad")
    selector = inputs[0]
    descriptors = inputs[1]
    expertIndices = outputs[0]
    selectorsByDescriptor = tf.dynamic_partition(selector, tf.to_int32(expertIndices), expertCount)
    descriptorGrad = tf.map_fn(_gradCalc, (selectorsByDescriptor, descriptors), dtype=tf.float32)
    return None, descriptorGrad


def _selectExpertsGrad(op, grad):
    print("selectExpertsGrad")
    descriptors = op.inputs[1]
    expertCount = tf.shape(descriptors)[0]
    return _selectExpertsGradOp(op.inputs, op.outputs, expertCount)
    # selector = op.inputs[0]
    # descriptors = op.inputs[1]
    # expertIndices = op.outputs[0]
    # expertCount = tf.shape(descriptors)[0]
    # selectorsByDescriptor = tf.dynamic_partition(selector, expertIndices, expertCount)
    # descriptorGrad = tf.map_fn(_gradCalc, tf.range(0, (tf.transpose(selectorsByDescriptor), tf.transpose(descriptors))), dtype=(tf.float32, tf.float32))
    # return None, descriptorGrad