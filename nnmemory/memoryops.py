import tensorflow as tf
import numpy as np

def selectExpertsOP(selector, descriptors, expertCount, name=None):

    with tf.name_scope(name):
        #Normalize to make the comparison meaningful
        normDesc = tf.transpose(tf.nn.l2_normalize(descriptors, 1))
        normSelector = tf.nn.l2_normalize(selector, 1)
        #We don't want to change the selector on backprop, just the descriptor. The selector should learn normal features.
        normSelector = tf.stop_gradient(normSelector)
        #Now that everything is normalized, take the dot product to test similarity. The closer to '1' the more similar
        # a descriptor is.
        selectorSimilarity = tf.matmul(normSelector,
                                       normDesc)

        # Now test to see which was the most similar
        knowledgeIndices = tf.argmax(selectorSimilarity, axis=1)
    loss = tf.reduce_max(selectorSimilarity, axis=1)
    loss = 1.0 - loss
    return knowledgeIndices, loss

def grow(variable, currentKnowledgeSize, session):
    current = session.run(variable)
    newRow = np.mean(current[0:currentKnowledgeSize], axis=0)
    current[currentKnowledgeSize] = newRow
    session.run(tf.assign(variable, current))

def selectProbabalisticExpertsOP(selector, descriptors, sensitivity, name=None):

    with tf.name_scope(name):

        #Normalize to make the comparison meaningful
        normDesc = tf.transpose(tf.nn.l2_normalize(descriptors, 1))
        normSelector = tf.nn.l2_normalize(selector, 1)
        #We don't want to change the selector on backprop, just the descriptor. The selector should learn normal features.
        normSelector = tf.stop_gradient(normSelector)
        #Now that everything is normalized, take the dot product to test similarity. The closer to '1' the more similar
        # a descriptor is.
        selectorSimilarity = tf.matmul(normSelector,
                                       normDesc)

        # Now test to see which was the most similar
        #knowledgeIndices = tf.argmax(selectorSimilarity, axis=1)
        knowledgeIndices = tf.cond(tf.equal(sensitivity,0.0), lambda: tf.argmax(selectorSimilarity, axis=1), lambda: tf.reshape(tf.multinomial(tf.log(selectorSimilarity) * 1.0/sensitivity, 1), [-1]))
        #knowledgeIndices = tf.reshape(tf.multinomial(tf.log(selectorSimilarity) * 1.0/sensitivity, 1), [-1])
    loss = tf.reduce_max(selectorSimilarity, axis=1)
    loss = 1.0 - loss
    return knowledgeIndices, loss


def expertLayer(x, knowledgeIndices, expertWeights, expertBiases, expertCount):
    knowledgeIndices = tf.to_int32(knowledgeIndices)
    xByDescriptor = tf.dynamic_partition(x, knowledgeIndices, expertCount)
    xIndices = tf.dynamic_partition(tf.range(0, tf.shape(x)[0]), knowledgeIndices, expertCount)
    # mergedLayer = tf.tensordot(xByDescriptor, expertWeights, ([1],[1])) + expertBiases
    mergedLayer = []
    for i in range(expertCount):
        mergedLayer.append(tf.matmul(xByDescriptor[i], expertWeights[i]) + expertBiases[i])
    return tf.nn.relu(tf.dynamic_stitch(xIndices, mergedLayer))

