import os

os.environ['MLP_DATA_DIR'] = '../School/MLP/MLP_DATA/'
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf
from mlp.data_providers import CIFAR10DataProvider, CIFAR100DataProvider


class TestingEnvironment:
    def __init__(self, batchSize=50):

        self.dataSources = {}
        self.dataSources["cifar10_train"] = CIFAR10DataProvider('train', batch_size=batchSize)
        self.dataSources["cifar10_valid"] = CIFAR10DataProvider('valid', batch_size=batchSize)
        self.dataSources["cifar100_train"] = CIFAR100DataProvider('train', batch_size=batchSize)
        self.dataSources["cifar100_valid"] = CIFAR100DataProvider('valid', batch_size=batchSize)
        self.resetTrain("cifar100")
        self.resetValid("cifar100")

    def resetTrain(self, dataSet, setDefault=True):
        self.dataSources["{}_train".format(dataSet)].reset()
        if setDefault:
            self.train = self.dataSources["{}_train".format(dataSet)]

    def resetValid(self, dataSet, setDefault=True):
        self.dataSources["{}_valid".format(dataSet)].reset()
        if setDefault:
            self.valid = self.dataSources["{}_valid".format(dataSet)]


def validate(dataSet, accuracyOp, xPlacehloder, labelsPlaceholder, keepProb):
    acc = 0.0
    numBatches = 0
    for batch in dataSet.valid:
        numBatches += 1
        acc += accuracyOp.eval(feed_dict={xPlacehloder: batch[0], labelsPlaceholder: batch[1], keepProb: 1.0})
    return acc / numBatches


def main():
    BATCH_SIZE = 100

    sess = tf.InteractiveSession()
    # dataSet = input_data.read_data_sets('MNIST_data', one_hot=True)
    dataSet = TestingEnvironment(batchSize=BATCH_SIZE)

    SELECTOR_WIDTH = 16
    TOTAL_WIDTH = 256
    EXPERT_WIDTH = TOTAL_WIDTH - SELECTOR_WIDTH
    CLASS_COUNT = dataSet.train.num_classes
    INPUT_DIM = dataSet.train.inputs.shape[1]

    x = tf.placeholder(tf.float32, shape=[None, INPUT_DIM])
    labels = tf.placeholder(tf.float32, shape=[None, CLASS_COUNT])
    keep_prob = tf.placeholder(tf.float32)

    # Define the selector layer
    selectorWeights = tf.Variable(tf.truncated_normal([INPUT_DIM, SELECTOR_WIDTH], stddev=0.1))
    selectorBias = tf.Variable(tf.zeros([SELECTOR_WIDTH]))
    selectorLayer = tf.nn.relu(tf.matmul(x, selectorWeights) + selectorBias)

    expertKnowledgeWeights = tf.Variable(tf.truncated_normal([INPUT_DIM, EXPERT_WIDTH], stddev=0.1))
    expertKnowledgeBias = tf.Variable(tf.zeros([EXPERT_WIDTH]))
    expertLayer = tf.nn.relu(tf.matmul(x, expertKnowledgeWeights) + expertKnowledgeBias)

    layer1 = tf.concat([selectorLayer, expertLayer], axis=1)

    outputWeights = tf.Variable(tf.truncated_normal([TOTAL_WIDTH, CLASS_COUNT], stddev=0.1))
    outputBiases = tf.Variable(tf.zeros([CLASS_COUNT]))
    logits = tf.matmul(layer1, outputWeights) + outputBiases

    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=labels))
    train = tf.train.GradientDescentOptimizer(.1).minimize(loss)

    accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(logits, 1), tf.argmax(labels, 1)), tf.float32))
    sess.run(tf.global_variables_initializer())

    lastStep = 0
    lastEval = 0
    lastSubStep = 0

    EPOCHS = 40
    SUB_STEP_SIZE = BATCH_SIZE * 100
    STEP_SIZE = SUB_STEP_SIZE * 10
    EVAL_SIZE = STEP_SIZE * 2
    i = 0
    for epoch in range(EPOCHS):
        for batch in dataSet.train:
            # batch = dataSet.train.next_batch(BATCH_SIZE)
            train.run(feed_dict={x: batch[0], labels: batch[1], keep_prob: .5})
            lastEval += BATCH_SIZE
            lastStep += BATCH_SIZE
            lastSubStep += BATCH_SIZE

            if lastEval >= EVAL_SIZE:
                # train_accuracy = accuracy.eval(feed_dict={x: dataSet.valid.inputs, labels: dataSet.valid.targets, keep_prob: 1.0})
                train_accuracy = validate(dataSet, accuracy, x, labels, keep_prob)
                print("{}, step {}, test accuracy {:0.2f}".format(epoch, i + BATCH_SIZE, train_accuracy * 100.0))
                lastEval -= EVAL_SIZE
            elif lastStep >= STEP_SIZE:
                train_accuracy = accuracy.eval(feed_dict={x: batch[0], labels: batch[1], keep_prob: 1.0})
                print("{}, {}, training accuracy {:0.2f}".format(epoch, i + BATCH_SIZE, train_accuracy * 100.0))
                lastStep -= STEP_SIZE
            if lastSubStep >= SUB_STEP_SIZE:
                print("{}, {}".format(epoch, i + BATCH_SIZE))
                lastSubStep -= SUB_STEP_SIZE
            i += batch[0].shape[0]

    print("Test accracy {}".format(validate(dataSet, accuracy, x, labels, keep_prob)))
    # print("Test accracy {}".format(accuracy.eval(feed_dict={x: dataSet.valid.inputs, labels: dataSet.valid.targets, keep_prob: 1.0})))


if __name__ == '__main__':
    main()
